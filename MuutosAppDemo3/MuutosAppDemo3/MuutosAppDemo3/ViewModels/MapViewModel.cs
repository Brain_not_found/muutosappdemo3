﻿// Using System
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;

// Using Xamarin
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Essentials;

// Using nuget packages
using Newtonsoft.Json;

// Using Project
using MuutosAppDemo3.Models;
using MuutosAppDemo3.Interfaces;

namespace MuutosAppDemo3.ViewModels
{
    public class MapViewModel
    {
        #region properties
        // Utilisation de models
        public CameraPosition cameraPosition { get; set; }
        private Assembly assembly { get; set; }
        private Xamarin.Forms.GoogleMaps.Map map { get; set; }

        public UserMap userMap = new UserMap();
        public UserCameraPosition userCameraPosition = new UserCameraPosition();

        // Variables partagées du ViewModel
        public string selectedStyle { get; private set; }

        // Variables locales du viewModel
        private Dictionary<string, Stream> streamDictionary { get; set; }
        private string[] ressourcesFilesNames { get; set; }
        #endregion

        #region constructor
        public MapViewModel() { }
        public MapViewModel(Xamarin.Forms.GoogleMaps.Map map)
        {
            // Instanciation de variables
            streamDictionary = new Dictionary<string, Stream>();

            this.map = map;

            #region ApplyMapStyle // Récupération des styles de maps fournis dans les ressources + application du premier

            // Déclaration de l'assembly
            assembly = typeof(MapViewModel).GetTypeInfo().Assembly;

            // Récupération du nom de toutes les ressources liées à cet assembly
            ressourcesFilesNames = assembly.GetManifestResourceNames();

            foreach (string ressource in ressourcesFilesNames)
            {
                // Mise en place du reader permettant de lire le contenu du fichier
                Stream stream = assembly.GetManifestResourceStream(ressource);
                StreamReader reader = new StreamReader(stream);

                // Lecture du fichier Json
                string editorMapStyle = reader.ReadToEnd();

                // Ajout du style à la liste de styles du model
                userMap.Styles.Add(editorMapStyle);
            }

            map.MapStyle = MapStyle.FromJson(userMap.Styles[0]);
            #endregion

            // Récupération de la dernière position de caméra enregistrée
            GetLastCameraPosition();
        }
        #endregion

        #region functions
        public void ChangeMapStyle()
        {
            try  // Try afin de supporter une erreur dans la récupération de l'index
            {
                // Récupération de l'index dans la liste du style sélectionné
                int selectedStyleNumber = userMap.Styles.IndexOf(selectedStyle);

                // Si l'index n'indique pas le dernier élément de la liste de styles
                if (selectedStyleNumber < userMap.Styles.Count - 1)
                    selectedStyleNumber++;
                else
                    selectedStyleNumber = 0;

                selectedStyle = userMap.Styles[selectedStyleNumber];
            }
            catch (Exception ex)
            {
                DependencyService.Get<IMessage>().LongAlert("Erreur lors de la récupération d'un nouveau style");
            }
        }

        public void MoveToLastPosition()
        {
            // Déplacement de la caméra
            //map.AnimateCamera(CameraUpdateFactory.NewCameraPosition(cameraPosition));
            map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(cameraPosition.Target.Latitude, cameraPosition.Target.Longitude), Distance.FromKilometers(1)));
        }

        public async void GetLastCameraPosition()
        {
            try  // Afin de supporter une erreur dans la récupération depuis le SecureStorage
            {
                // Récupération des données depuis le stockage
                string lastCameraPositionJSON = await SecureStorage.GetAsync("userTargetLat");
                UserCameraPosition lastCameraPosition = JsonConvert.DeserializeObject<UserCameraPosition>(lastCameraPositionJSON);

                // Conversion des données récupérées en objet CameraPosition
                cameraPosition = lastCameraPosition.ConvertToCameraPosition();
            }
            catch
            {
                DependencyService.Get<IMessage>().LongAlert("Erreur lors de la récupération de la dernière position de la caméra");
            }
        }

        public async void SaveUserCameraPosition(CameraPosition cameraPosition)
        {
            // Modification de userCameraPosition
            userCameraPosition.TargetLat = cameraPosition.Target.Latitude;
            userCameraPosition.TargetLong = cameraPosition.Target.Longitude;
            userCameraPosition.Zoom = cameraPosition.Zoom;
            userCameraPosition.Bearing = cameraPosition.Bearing;
            userCameraPosition.Tilt = cameraPosition.Tilt;

            // Si le dictionnaire de streams contient déjà le userCameraPositionStream
            if (!streamDictionary.ContainsKey("userCameraPositionStream"))
            {
                Stream stream = assembly.GetManifestResourceStream("MuutosAppDemo3.Ressources.Data.UserData.json");
                streamDictionary.Add("userCameraPositionStream", stream);
            }

            try  // Afin de supporter une erreur dans la sauvegarde dans SecureStorage
            {
                await SecureStorage.SetAsync("userTargetLat", userCameraPosition.ConvertToJson());
                DependencyService.Get<IMessage>().ShortAlert("Sauvegarde de la position efféctuée");
            }
            catch (Exception ex)
            {
                DependencyService.Get<IMessage>().LongAlert("Erreur lors de l'enregistrement de la position de la caméra");
            }
        }
        #endregion
    }
}

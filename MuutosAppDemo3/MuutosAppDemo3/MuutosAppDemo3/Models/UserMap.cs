﻿// Using System
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

// Using Nuget
using Newtonsoft.Json;

namespace MuutosAppDemo3.Models
{
    public class UserMap
    {
        #region properties
        public List<string> Styles { get; set; }
        #endregion

        #region Constructor
        public UserMap()
        {
            Styles = new List<string>();
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.GoogleMaps;
using Newtonsoft.Json;

namespace MuutosAppDemo3.Models
{
    public class UserCameraPosition
    {
        #region properties
        public double TargetLat { get; set; }
        public double TargetLong { get; set; }
        public double Zoom { get; set; }
        public double Bearing { get; set; }
        public double Tilt { get; set; }
        #endregion

        #region Constructor
        public UserCameraPosition() { }
        #endregion

        #region function
        // Renvoie l'objet en format JSON
        public string ConvertToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        // Renvoie l'objet converti en objet CameraPosition
        public CameraPosition ConvertToCameraPosition()
        {
            Position position = new Position(TargetLat, TargetLong);
            CameraPosition cameraPosition = new CameraPosition(position, Zoom, Bearing, Tilt);

            return cameraPosition;
        }
        #endregion
    }
}

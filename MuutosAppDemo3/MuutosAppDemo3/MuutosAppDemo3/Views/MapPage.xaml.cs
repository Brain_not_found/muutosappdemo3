﻿// Using System
using System;
using System.Collections.Generic;
using System.Linq;

// Using Xamarin
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Essentials;

// Using Project
using MuutosAppDemo3.ViewModels;
using MuutosAppDemo3.Interfaces;

// Using packages
using Newtonsoft.Json;

namespace MuutosAppDemo3.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapPage : ContentPage
    {
        #region properties
        public MapViewModel viewModel { get; }
        #endregion

        #region constructor
        public MapPage()
        {
            InitializeComponent();

            // Attribution du viewModel à la page xaml 
            BindingContext = viewModel = new MapViewModel(map);

            #region Timer // Mise en place d'une action répétée toutes les 15 secondes
            TimeSpan seconds = TimeSpan.FromSeconds(15);
            Device.StartTimer(seconds, () =>
            {
                // Sauvegarde de la position de la caméra
                //DependencyService.Get<IMessage>().ShortAlert("TODO - Sauvegarder la position");
                viewModel.SaveUserCameraPosition(map.CameraPosition);
                return true;
            });
            #endregion
        }
        #endregion

        #region events
        protected override void OnAppearing()
        {
            base.OnAppearing();

            viewModel.MoveToLastPosition();
        }

        private void ChangeMapStyle(object sender, EventArgs e)
        {
            viewModel.ChangeMapStyle();

            // Application du style de map
            map.MapStyle = MapStyle.FromJson(viewModel.selectedStyle);
        }
        #endregion
    }
}
﻿// Using Android
using Android.App;
using Android.Widget;

// Using Project
using MuutosAppDemo3.Interfaces;

[assembly : Xamarin.Forms.Dependency(typeof(MuutosAppDemo3.Droid.SpecialEvents.AndroidToast))]
namespace MuutosAppDemo3.Droid.SpecialEvents
{
    public class AndroidToast : IMessage
    {
        public void LongAlert(string message)
        {
            Toast.MakeText(Application.Context, message, ToastLength.Long).Show();
        }

        public void ShortAlert(string message)
        {
            Toast.MakeText(Application.Context, message, ToastLength.Short).Show();
        }
    }
}